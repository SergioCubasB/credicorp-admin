import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Auth } from '../interface/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private env = environment;
  protected endpoint: string;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.endpoint = `${environment.api}/v1`;
  }

  postLogin(auth: any){
    return this.http.post(`${this.endpoint}/user-admin/signin`, auth);
  }

  get isLoggedIn(): boolean {
    const user = localStorage.getItem('_TK');
    return user !== 'null' ? true : false;
  }

  logout(): void{
    localStorage.clear();
    this.router.navigate(['/']);
  }
  
}
