import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material/material.module';

/* Componentes propios */
import { AuthComponent } from './auth.component';
import { AuthRouting } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PasswordComponent } from './password/password.component';

@NgModule({
  declarations: [
    LoginComponent,
    AuthComponent,
    PasswordComponent,
  ],
  exports: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,

     /* Rutas propias */
    AuthRouting
  ]
})
export class AuthModule { }
