import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import { ControlBase } from './../control-base';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-html',
  templateUrl: './html.component.html',
  styleUrls: ['./html.component.scss']
})
export class HtmlComponent implements OnInit {
  public Editor = ClassicEditor;

  ckconfig = {
    toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
  };


  @Input() meta!: ControlBase;
  @Input() form!: FormGroup;

  constructor() { 
  }

  ngOnInit(): void {
  }

}
