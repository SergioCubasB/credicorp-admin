import { FormGroup } from '@angular/forms';
import { ControlBase } from './../control-base';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss']
})
export class DateInputComponent implements OnInit {
  @Input() meta!: ControlBase;
  @Input() form!: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

}
