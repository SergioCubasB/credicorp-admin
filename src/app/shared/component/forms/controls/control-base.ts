export interface ControlBase {
  language: string;
  field: string;
  label?: string;
  key: string;
  data: any[];
  required?: boolean;
  order: number;
  controlType: string;
  class?: string;
}
