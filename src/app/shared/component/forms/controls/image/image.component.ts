import { ControlBase } from './../control-base';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  @Input() meta!: ControlBase;
  @Input() form!: FormGroup;

  @ViewChild('fileInput') fileInput!: ElementRef;

  showActions: boolean = false;
  preview: any;
  myfilename: any;
  btnPortada: boolean = true;
  public imageFile:any = [];

  constructor() { }

  ngOnInit(): void {}

  resetFileEvent(){
    this.showActions = true;

    let inputElement: HTMLElement = this.fileInput.nativeElement as HTMLElement;
    inputElement.click();
  }


  fileChangeEvent(fileInput: any) {
    var fileTypes = ['jpg', 'jpeg', 'png']; 
    var extension = fileInput.target.files[0].name.split('.').pop().toLowerCase();
    
    if (fileInput.target.files && fileInput.target.files[0] && fileTypes.indexOf(extension) > -1) {
      this.myfilename = '';
      Array.from(fileInput.target.files).forEach((file: any) => {
        this.myfilename += file.name;
      });

      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageFile.push(fileInput.target.files[0]);

        this.btnPortada = false;

        const image = new Image();
        image.src = e.target.result;
        
        image.onload = rs => {

          this.showActions = true;
          this.preview = e.target.result;
        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
      this.fileInput = fileInput.target.files[0];

    } else {
      Swal.fire('Error al subir imagen', 'Se debe subir una imagen en cualquiera de los siguientes formato: JPG, JPEG, PNG.', 'warning');
      this.myfilename = 'Select File';
    }

  }

}
