import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownMultiplyComponent } from './dropdown-multiply.component';

describe('DropdownMultiplyComponent', () => {
  let component: DropdownMultiplyComponent;
  let fixture: ComponentFixture<DropdownMultiplyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DropdownMultiplyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMultiplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
