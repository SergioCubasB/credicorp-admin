import { SharedService } from './../../../../services/shared.service';
import { DropDownControl } from './../dropdown/dropdown-control';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dropdown-multiply',
  templateUrl: './dropdown-multiply.component.html',
  styleUrls: ['./dropdown-multiply.component.scss']
})
export class DropdownMultiplyComponent implements OnInit {
  @Input() meta!: DropDownControl;
  @Input() form!: FormGroup;

  options: any[] = [];

  constructor(
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {    
    const { key } = this.meta;
    
    switch (key) {
      case 'speakers':
          this.sharedService.getTypeUser('1').subscribe(
            (response:any) => {
              const { data } = response.data;

              var arrayTransform:any = [];
              data.forEach(element => {

                arrayTransform.push(
                  {
                    id: element.id * 1,
                    name : element.name
                  }
                )
              });
              this.options = arrayTransform;
            }
          )
        break;
      case 'moderators':
        this.sharedService.getTypeUser('2').subscribe(
          (response:any) => {
            const { data } = response.data;
            var arrayTransform:any = [];
              data.forEach(element => {

                arrayTransform.push(
                  {
                    id: element.id * 1,
                    name : element.name
                  }
                )
              });
              this.options = arrayTransform;
          }
        )
          break;
      default:
        break;
    }
    
  }

  getType(){

  }


}
