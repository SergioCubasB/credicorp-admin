import { ControlBase } from '../control-base';

export interface DropDownControl extends ControlBase {
  options?: { name: string; id: string }[];
}

