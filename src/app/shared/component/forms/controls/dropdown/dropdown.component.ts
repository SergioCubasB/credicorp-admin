import { ParametersService } from './../../../../../pages/dashboard/modules/parameters/services/parameters.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DropDownControl } from './dropdown-control';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent implements OnInit {
  @Input() meta!: DropDownControl;
  @Input() form!: FormGroup;

  options: any[] = [];
  constructor(
    private parametersService: ParametersService
  ) {}

  ngOnInit(): void {
    const PARAMETER:any = this.meta.options;
    this.getParameter(PARAMETER);
  }

  getParameter( type:string ){
    this.parametersService.getGroup(type)
    .subscribe(
      (response: any) => {
        const { data } = response.data;
        var arrayTransform:any = [];
        data.forEach(element => {

          arrayTransform.push(
            {
              id: element.value * 1,
              name : element.name
            }
          )
        });
        
        this.options = arrayTransform;

      }
    )
  }
}
