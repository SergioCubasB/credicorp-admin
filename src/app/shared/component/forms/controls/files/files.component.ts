import { ControlBase } from './../control-base';
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
  @Input() meta!: ControlBase;
  @Input() form!: FormGroup;

  @ViewChild('fileInput') fileInput!: ElementRef;

  showActions: boolean = false;
  preview: any;
  myfilename: any;
  btnPortada: boolean = true;
  public imageFile:any = [];

  max: boolean = true;

  filesSendNames: any[] = [];
  filesSend: any[] = [];

  base64String: string = '';
  name: string = '';
  imagePath: string = '';
 
  constructor() { }

  ngOnInit(): void {}

  removeImageEdit(i, imagepath) {
    this.form.value.id = i;
    this.form.value.ImagePath = imagepath;
  }

  removeImage(i) {
    this.filesSend.splice(i, 1);
    this.max = true;
  }

  onSelectFile(file) {
    if (file.target.files && file.target.files[0]) {
      var filesAmount = file.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {

          this.filesSendNames.push({
            name: file.target.files[i]
          })

          this.filesSend.push({ 
            file: file.target.files[i]
          });

        }
        
        reader.readAsDataURL(file.target.files[i]);
      }

    }

    if(this.filesSend.length === 2){
      this.max = false;
    }

  }

}
