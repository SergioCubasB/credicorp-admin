import { AuthService } from './../../../../auth/service/auth.service';
import { selectLoading } from './../../../../state/selectors/events.selectors';
import { loadedEvents, loadEvents } from './../../../../state/actions/events.actions';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { SharedService } from './../../../services/shared.service';
import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ControlBase } from '../controls/control-base';
import Swal, { SweetAlertIcon } from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
})
export class DynamicFormComponent implements OnChanges, OnInit {
  //* Generar campos del formulario */
  @Input() meta: ControlBase[] = [];

  //* Llenar formulario */
  @Input() data: any = {};

  @Input() idElement: string = '1';
  @Input() title: any = "No tiene titulo";
  @Input() buttonText: any = "";
  @Input() type: any = "";
  @Input() service: any = "";
  @Input() anyData: any = false;

  form!: FormGroup;
  formData = new FormData();

  payLoad = '';
  submitted = false;

  //** Si tiene file */
  @ViewChild('fileInput') fileInput!: ElementRef;
  public imageFile:any = [];
  showActions: boolean = false;
  showActions_en: boolean = false;
  preview: any;
  preview_en: any;

  myfilename: any;
  btnPortada: boolean = true;
  filedata:any;
  presentationId: string = '';

  /** Files */
  max: boolean = true;
  filesSendNames: any[] = [];
  filesSend: any = [];
  filesRemovedSend: any = [];

  base64String: string = '';
  name: string = '';
  imagePath: string = '';


  //** Procesando petición */
  showPercentage: boolean = false;
  showMessage: string = 'Procesando...';
  
  constructor( 
    private store: Store<AppState>,
    private router: Router,

    private dialog: MatDialog,
    private sharedService: SharedService,
    private authService: AuthService
   ) {}
  
  ngOnInit(){

    if(this.type === 'PUT'){
      this.showPercentage = true;
      this.showMessage = "Cargando data...";
      
      if(this.anyData){
        this.showPercentage = false;
        this.form = this.toFormGroup(this.meta, '');
      }else{
        this.sharedService.getData(this.service, this.idElement).subscribe(
          (response:any) => {
            this.showPercentage = false;
            const { data } = response.data;
            
            if(data){
              if(data.hasOwnProperty('documents') && data.documents.length >= 3){    
                this.max = false;
              }
            }
             
            this.form = this.toFormGroup(this.meta, data);
          }
        )
      }
      
    }else{
      this.form = this.toFormGroup(this.meta, '');
    }

  }

  ngOnChanges() {    
    this.form = this.toFormGroup(this.meta, this.data);
  }

  toFormGroup(controls: ControlBase[], data: any) {
    const group: any = {};
    
    controls.forEach((language:any) => {
      language.data.forEach((control:any) => {

          if(this.type === 'PUT'){
            this.showActions = false;
            this.showActions_en = false;
          }
  
          if(control.key === 'date'){            
            group[control.key] = new FormControl(data[control.key]);
          }

          if(control.key === 'start_date'){
            group["start_date"] = new FormControl(data[control.key]);
          }

          if(control.key === 'end_date'){
            group[control.key] = new FormControl(data[control.key]);
          }

          if(control.key === 'presentations[0][url]'){
            //this.presentationId = 
            let dataResponse = "";
            if(data){
              dataResponse = data.presentations.url;
              this.presentationId = data.presentations.id;
            }
            group['presentations[0][url]'] = new FormControl(dataResponse);
          }

          if(control.key === 'presentations[0][url_en]'){
            let dataResponse = "";
            if(data){
              dataResponse = data.presentations.url_en;
            }
            group['presentations[0][url_en]'] = new FormControl(dataResponse);
          }
          
          if(control.key === 'documents'){
            const { documents } = data;
  
            if(documents){
              this.preview = "http://159.223.201.87/"+documents.route;
            }
  
            group['documents'] = new FormControl();
          }

          if(control.key === 'file'){
            const { file } = data;
  
            if(file){
              this.preview = "http://159.223.201.87/"+file.route;
            }
  
            group['file'] = new FormControl();
          }

          if(control.key === 'file_en'){
            const { file } = data;
  
            if(file){
              this.preview_en = "http://159.223.201.87/"+file.route_en;
            }
  
            group['file'] = new FormControl();
          }
  
          if(control.key === 'speakers'){
            var arrayTransform:any = [];

            if(control.key){
              if(data["speaker"]){
                data["speaker"].forEach(element => {
                  arrayTransform.push(element.id * 1)
                });
              }
              group['speakers'] = new FormControl(arrayTransform);
            }
            
          }
  
          if(control.key === 'moderators'){
            var arrayTransform:any = [];

            if(control.key){
              if(data["moderator"]){
                data["moderator"].forEach(element => {
                  arrayTransform.push(element.id * 1)
                });
              }
              
              group['moderators'] = new FormControl(arrayTransform);
            }
  
          }

          if(control.key === 'documents'){
            var arrayTransform:any = [];

            if(control.key){
              if(data["documents"]){
                data["documents"].forEach(element => {
                  element.isService = true;
                  arrayTransform.push(element);
                });
              }
              
              this.filesSend = arrayTransform;
            
              group['documents'] = new FormControl('');
            }
  
          }
          
          if (control.controlType !== 'label' 
            && control.key != 'file'
            && control.key != 'speakers'
            && control.key != 'moderators'
            && control.key != 'documents'
            && control.key != 'date'
            && control.key != 'start_date'
            && control.key != 'presentations[0][url_en]'
            && control.key != 'presentations[0][url]'
            && control.key != 'end_date') {
            group[control.key] = control.required
            ? new FormControl(data[control.key] || '', Validators.required)
            : new FormControl(data[control.key] || '');
          }

      });
    });
    
    return new FormGroup(group);
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit() {
    var isFile = false;
    this.submitted = true;
    
    /*if (this.form.invalid) {
      return;
    }*/
    this.showPercentage = true;
    const formRawValue = this.form.getRawValue();    
    
    Object.entries(formRawValue).forEach((key:any) => {
      switch (key[0]) {
        case 'speakers':
          key[1].forEach( (element:string, index:number) => {
            this.formData.append(`speakers[${index}]`, element);
          });
        break;

        case 'moderators':
          key[1].forEach( (element:string, index:number) => {
            this.formData.append(`moderators[${index}]`, element);
          });
        break;
        case 'documents':
          
          this.formData.append(`presentations[0][id]`, this.presentationId);

          var counter = 0;
          this.filesSend.forEach( (element, index) => {
            if(element.lastModified){
              this.formData.append(`documents[${counter}][file]`, element);
              this.formData.append(`documents[${counter}][name]`, element.name);
              isFile = true;
              counter++;
            }
          });         
          
          if(this.filesRemovedSend.length > 0){
            for (const filesRemoved in this.filesRemovedSend) {
              this.formData.append(`documents_del[${filesRemoved}]`, this.filesRemovedSend[filesRemoved].id); 
            }
            isFile = true;
          }

        break;
        
        case 'file':
          //this.formData.append('file', this.filedata);
        break;
        case 'file_en':
          //this.formData.append('file', this.filedata);
        case 'file_footer':
          //this.formData.append('file', this.filedata);
        break;
        case 'date':
          this.formData.append(key[0], this.formatDate(key[1]));
        break;
        case 'start_date':
          this.formData.append(key[0], this.formatDate(key[1]));
        break;
        case 'end_date':
          this.formData.append(key[0], this.formatDate(key[1]));
        break;
        default:
          this.formData.append(key[0], key[1]);
        break;
      }

    });
    
    this.store.dispatch(loadEvents());
    
    switch (this.type) {
      case 'PUT':
         
          if(isFile || formRawValue.hasOwnProperty('file') || formRawValue.hasOwnProperty('documents')){
            this.sharedService.postDataFile(this.service, this.formData, this.idElement).subscribe(
              (data:any) => {

                const { status, mensaje } = data.data;
                this.swalModal(status, mensaje, '');

              })
              return;
          }
          
          this.sharedService.putData(this.service, formRawValue, this.idElement).subscribe(
            (data:any) => {

              const { status, mensaje } = data.data;
              this.swalModal(status, mensaje, '');

            })
        break;
      case 'POST':
        
          this.sharedService.postData(this.service, this.formData).subscribe(
            (data:any) => {

              this.showPercentage = false;
              this.showMessage = "Creando registro...";
              
              const { status, mensaje } = data.data;
              this.swalModal(status, mensaje, '');

              if(status === 0){
                this.showPercentage = false;
              }

          })
        break;
      default:
        break;
    }
    
  }

  formatDate(date:any){
    const dateFormat = new Date(date);
    let formatted_date = dateFormat.getFullYear() + "-" + (dateFormat.getMonth() + 1) + "-" + dateFormat.getDate() + " " + dateFormat.getHours() + ":" + dateFormat.getMinutes() + ":" + dateFormat.getSeconds() 
    return formatted_date;
  }

  fileChangeEvent(fileInput: any, formControlName) {
    var fileTypes = ['jpg', 'jpeg', 'png']; 
    var extension = fileInput.target.files[0].name.split('.').pop().toLowerCase();
    
    if (fileInput.target.files && fileInput.target.files[0] && fileTypes.indexOf(extension) > -1) {
      this.myfilename = '';
      Array.from(fileInput.target.files).forEach((file: any) => {
        this.myfilename += file.name;
      });

      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageFile.push(fileInput.target.files[0]);

        this.btnPortada = false;

        const image = new Image();
        image.src = e.target.result;
        
        image.onload = rs => {
        };

        if(formControlName === 'file'){
          this.showActions = true;
          this.preview = e.target.result;
        }else if(formControlName === 'file_en' || formControlName === 'file_footer'){
          this.showActions_en = true;
          this.preview_en = e.target.result;
        }
      };

      reader.readAsDataURL(fileInput.target.files[0]);
      this.filedata = fileInput.target.files[0];

      this.formData.append(formControlName, this.filedata);

    } else {
      this.swalModal(400, 'Error al subir imagen', 'Se debe subir una imagen en cualquiera de los siguientes formato: JPG, JPEG, PNG.');
      this.myfilename = 'Select File';
    }

  }

  resetFileEventMultiply(formControlName){
    if(formControlName === 'file'){
      this.preview = "";
      this.showActions = false;

    }else if(formControlName === 'file_en' || formControlName === 'file_footer'){
      this.preview_en = "";
      this.showActions_en = false;

    }
  }

  resetFileEvent(){
    this.showActions = false;
    this.showActions_en = true;

    this.preview = "";
  }

  swalModal(status:number, tittle:string, message: string){
    var type: SweetAlertIcon = 'success';

    switch (status) {
      case 400:
          type = 'warning';
          Swal.close();
          this.showPercentage = false;
        break;
      case 200:
          this.dialog.closeAll();
          type = 'success';

          if(parseInt(this.idElement) === 0){
            this.authService.logout();

            Swal.fire("Contraseña actualizada.", "Vuelva a ingresar sus credenciales.", "success");
            return;
          }5
         

          this.sharedService.refreshStore(this.service)
          .subscribe(
            (response:any) => {
              const { data } = response.data;
              this.store.dispatch(loadedEvents( { event: data }));
            }
          )

        break;
      default:
        break;
    }
    Swal.fire(tittle, message, type);
  }












  removeImageEdit(i, imagepath) {
    
    this.form.value.id = i;
    this.form.value.ImagePath = imagepath;
  }

  removeImage(i) {

    this.filesSend = this.filesSend.filter(
      (element, index) => {

        if(element.id === i && element.isService){
          this.filesRemovedSend.push(element);
        }

        return element.id != i
      }
    )
    
    this.max = true;
  }

  onSelectFile(file) {
    
    if (file.target.files && file.target.files[0]) {
      
      var filesAmount = file.target.files.length;

     
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          file.target.files[i].id = this.filesSend.length + 1;
          file.target.files[i].isService = false;
          this.filesSend.push(file.target.files[i]);
        }
        
        reader.readAsDataURL(file.target.files[i]);
      }

    }
    
    if(this.filesSend.length === 2){    
      this.max = false;
    }

  }
  
  openFile(route){
    if(!route){
      return;
    }
    
    const url = this.router.serializeUrl(
      this.router.createUrlTree([route])
    );
  
    window.open(url, '_blank');
  }

  cancel(){
    this.dialog.closeAll();
  }
}
