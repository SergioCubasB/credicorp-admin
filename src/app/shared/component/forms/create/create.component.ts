import { Component, Inject, Input, VERSION } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {
  title = 'dynamic-form-app';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any = []
  ) {}
  
}
