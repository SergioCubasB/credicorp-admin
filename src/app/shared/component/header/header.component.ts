import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/auth/service/local-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public getScreenWidth: any;

  menu: boolean = true;
  isLoggin$:boolean = false;

  @Output() toggleSideBar = new EventEmitter<boolean>();

  constructor(
    private localStorageService: LocalStorageService,
    private route: Router
  ) {
    this.getScreenWidth = window.innerWidth;
   }

  ngOnInit(): void {
  }

  toggleMenu(){
    this.menu = !this.menu;
    this.toggleSideBar.emit(this.menu);
  }

}
