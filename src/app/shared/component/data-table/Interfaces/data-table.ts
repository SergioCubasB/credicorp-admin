export interface TableColumn {
    id: string;
    caption: string;
    field: string;
}