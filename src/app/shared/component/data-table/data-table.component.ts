import { loadedEvents, loadEvents } from './../../../state/actions/events.actions';
import { selectLoading } from 'src/app/state/selectors/events.selectors';
import { Store } from '@ngrx/store';
import { CreateComponent } from './../forms/create/create.component';
import { SharedService } from './../../services/shared.service';
import { EventsService } from './../../../pages/dashboard/modules/events/services/events.service';
import { ViewComponent } from './../modals/view/view.component';
import { MatDialog } from '@angular/material/dialog';

import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from "@angular/material/table";
import { TableColumn } from './Interfaces/data-table';
import  Swal  from 'sweetalert2';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AppState } from 'src/app/state/app.state';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit, OnDestroy{
  loading$: Observable<any> = new Observable(); 
  private _subscription:Subscription = new Subscription();

  data$: Observable<any> = new Observable(); 

  showPercentage: boolean = true;
  isChecked = true;
  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    //Servicios
    public eventsService: EventsService,
    private sharedService:  SharedService
  ){ }
  @Input() actions:any = { "editar": true, "eliminar": true };

  public _dataSource = new MatTableDataSource();
  public displayedColumns!: string[];
  @Input() columns!: TableColumn[];
  @Input() classModal: string = "";
  @Input() classWidthModal: string = "";


  @Input() meta!: any[];
  @Input() estado!: boolean;

  @Input() serviceGET: string = '';
  @Input() serviceGETDetail: string = '';


  @Input() set dataSource(data: any[]) { this.setDataSource(data); }
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  types: any[] = [];
  nationality: any[] = [];
  message: string = "";
  
  ngOnInit(): void {
    this.message = "Cargando data...";
    this.loading$ = this.store.select(selectLoading);

    this._subscription = this.loading$.subscribe( (data) =>{
        if(data){
          setTimeout(() => {
            this.displayedColumns = this.columns.map((tableColumn: TableColumn) => tableColumn.caption);

            this.showPercentage = false;
            this._dataSource.paginator = this.paginator;
          }, 1000);
        }
        
      }
    )

  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  setDataSource(data: any) {
    this.data$ = this.store.select('event');
    this.data$.subscribe(
      (data:any) => {
        const { event } = data;
        this._dataSource = new MatTableDataSource<any>(event);
      }
    )
  }

  /* Actions */
  openView(idService: string){
    
    const dialogRef = this.dialog.open(ViewComponent, {
      width: '400px',
      data: {
        id: idService,
        name: this.serviceGET,
        detail: this.serviceGETDetail
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });

  }

  openDelete(id:string){
    Swal.fire({
      title: '¿Seguro de eliminar este registro?',
      showDenyButton: true,
      showCancelButton: true,
      showConfirmButton: false,
      confirmButtonText: 'Save',
      denyButtonText: `Si, eliminar`,
      cancelButtonText: `Cancelar`
    }).then((result) => {
      this.store.dispatch(loadEvents());

      if (result.isDenied) {
        this.showPercentage = true;
        this.message = 'Eliminando registro...';

        this.sharedService.deleteData(this.serviceGET, id)
        .subscribe(
          (data:any) => {
            this.showPercentage = false;
            Swal.fire('Registro eliminado', '', 'success')

            this.sharedService.refreshStore(this.serviceGET)
              .subscribe(
              (response:any) => {
                const { data } = response.data;
                this.store.dispatch(loadedEvents( { event: data }));
              }
            )

          }
        )
      }
      
    })
  }

  openEdit(id:string){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: this.classWidthModal != '' ? this.classWidthModal : '800px',
      panelClass: this.classModal,
      data: {
        title: `Editar`,
        id: id,
        meta: this.meta,
        buttonText: "Actualizar",
        type: 'PUT',
        service: this.serviceGET
      }
    });
  }

  changeState(event: MatSlideToggleChange, id: string){
    if(event.checked){
      this.sharedService.changeState('event',id)
      .subscribe(
        (data:any) => {

          Swal.fire({
            title: 'Se cambio el estado exitosamente.',
            icon: 'success',
            denyButtonText: `Aceptar`,
            showDenyButton: true,
            showCancelButton: false,
            showConfirmButton: false,
          })
          
        }
      )
    }
    
  }

  openViewFile(image){
    var pathImage = "";
    const { route } = image;

    if(route){
      pathImage = 'http://159.223.201.87/'+route;
    }
    else {
      pathImage = '../../../../assets/image/img/sin-IMAGEN.jpg';
    }

    Swal.fire({
      imageUrl: pathImage,
      imageAlt: 'Imagen'
    })
  }

  validateType(value){
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this._dataSource.filter = filterValue.trim().toLowerCase();

    if (this._dataSource.paginator) {
      this._dataSource.paginator.firstPage();
    }
  }

}
