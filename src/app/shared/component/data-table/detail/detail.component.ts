import { Ng7MatBreadcrumbService } from 'ng7-mat-breadcrumb';
import { loadedEvents } from './../../../../state/actions/events.actions';
import { selectLoading, selectIselectListEvents } from './../../../../state/selectors/events.selectors';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { SharedService } from './../../../services/shared.service';
import { CreateComponent } from './../../forms/create/create.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];
  estadoData: boolean = false;
  id: any;
  service: any;


  message: string = "";
  /**Servicios */
  serviceGET: string = 'module/1/detail';
  serviceGETDetail: string = 'module/1/detail';
  constructor(
    private ng7MatBreadcrumbService: Ng7MatBreadcrumbService,
    private store: Store<AppState>,
    public dialog: MatDialog,
    private _Activatedroute:ActivatedRoute,

    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.id = this._Activatedroute.snapshot.paramMap.get("id");

    this.sharedService.getDataPageName(this.id).subscribe(
      (response: any) => {
        const { name } = response.data.data;
        
        const breadcrumb = { detail: name};
        this.ng7MatBreadcrumbService.updateBreadcrumbLabels(breadcrumb);
      }
    )

    this.message = "Cargando data...";
    this.store.select(selectLoading);

    this.myColumns = this.getColumns();
    this.getDetail();

  }

  getDetail(){
    this.id = this._Activatedroute.snapshot.paramMap.get("id");
    this.service = this._Activatedroute.snapshot.paramMap.get("page");
    
    this.sharedService.getDataPage(this.id, this.service)
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.store.select(selectIselectListEvents)
        .subscribe(
          (events) => {
            this.myData = events;
          }
        )
        
      }
    )
  }

  getColumns():  any[] {
    var columns: any[] = [];
    this.service = this._Activatedroute.snapshot.paramMap.get("page");
    this.id = this._Activatedroute.snapshot.paramMap.get("id");
    
    switch (this.service) {
      case 'detail':
        columns = [
          { caption: 'Titulo', field: 'title' },
          { caption: 'Descripcción', field: 'description' },
          { caption: 'Acciones', field: 'acciones' }];

          this.serviceGET = `module/${this.id}/detail`;
          this.serviceGETDetail = `module/${this.id}/detail`;
        this.getMetaDetail();
        break;
      case 'attribute':
          columns = [
          { caption: 'Nombre', field: 'name' },
          { caption: 'Imagen', field: 'file' },
          { caption: 'Acciones', field: 'acciones' }];

          this.serviceGET = `module/${this.id}/attribute`;
          this.serviceGETDetail = `module/${this.id}/attribute`;

          this.getMetaAttribute();
          break;
      case 'attribute-external':
          columns = [
            { caption: 'Name', field: 'name' },
            { caption: 'URL', field: 'url' },
            { caption: 'Acciones', field: 'acciones' }];
            
          this.serviceGET = `module/${this.id}/attribute-external`;
          this.serviceGETDetail = `module/${this.id}/attribute-external`;

          this.getMetaAttributeExternal();
          break;

      default:
        break;
    }
    return columns;

  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '900px',
      data: {
        title: "Crear",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: this.serviceGET
      }
    });
    
  }

  getMetaDetail(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "title",
            "required": true,
            "order": 1
          },
          {
            "label": "Descripcción:",
            "controlType": "htmleditor",
            "key": "description",
            "required": true,
            "order": 2
          },
          {
            "label": "Url:",
            "controlType": "textinput",
            "key": "url",
            "required": true,
            "order": 3
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "title_en",
            "required": true,
            "order": 2
          },
          {
            "label": "Descripcción:",
            "controlType": "htmleditor",
            "key": "description_en",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

  getMetaAttribute(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 2
          },
          {
            "label": "Imagen:",
            "controlType": "imageInput",
            "key": "file",
            "required": true,
            "order": 2
          },
          {
            "label": "Tipo:",
            "key": "type_attribute_id",
            "options": 'TYPE_ATTRIBUTE',
            "order": 8,
            "controlType": "dropdown"
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name_en",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

  getMetaAttributeExternal(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 2
          },
          {
            "label": "URL:",
            "controlType": "textinput",
            "key": "url",
            "required": true,
            "order": 2
          },
          {
            "label": "Tipo:",
            "key": "display_type_id",
            "options": 'DISPLAY_TYPE',
            "order": 8,
            "controlType": "dropdown"
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name_en",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

}
