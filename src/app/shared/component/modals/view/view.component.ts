import { SharedService } from './../../../services/shared.service';
import { EventsService } from './../../../../pages/dashboard/modules/events/services/events.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  data$: any = [];
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {id: string, name: string, detail: string},
    private sharedService:  SharedService
  ) { }


  ngOnInit(): void {
    this.getDetail();
  }

  getDetail(){
    const { id, name, detail  } = this.data;
    this.sharedService.getData(name, id)
    .subscribe(
      (response: any) => {
        const { data } = response.data;
        this.data$ = data;
      }
    )
  }

}
