import { AuthService } from './../../../auth/service/auth.service';
import { ViewComponent } from './../modals/view/view.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { CreateComponent } from '../forms/create/create.component';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  showFiller = true;
  meta: any[] = [];

  constructor(
    public dialog: MatDialog,

    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getMeta();
  }

  changePassword(){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '300px',
      data: {
        title: "Actualizar contraseña",
        meta: this.meta,
        buttonText: "Cambiar contraseña",
        id: 0,
        anyData: true,
        type: 'PUT',
        service: 'user-admin/update-password-admin'
      }
    });

  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Anterior contraseña:",
            "controlType": "passwordInput",
            "key": "password_old",
            "required": true,
            "order": 2
          },
          {
            "label": "Nueva contraseña:",
            "controlType": "passwordInput",
            "key": "password_new",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

  
  closeSession(){
    this.authService.logout();
  }
}
