import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SharedService {
    private env = environment;
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${this.env.api}/v1`;
    }

    refreshStore(name:string){
        return this.http.get(this.api + `/${name}`);
    }

    getData(name:string, id: string){
        return this.http.get(this.api + `/${name}/${id}?expand=speaker,moderator,presentations,file,documents`);
    }

    postData(name:string, data:any){
        return this.http.post(this.api + `/${name}`, data);
    }

    postDataFile(name:string, data:any, id:string){
        return this.http.post(this.api + `/${name}/${id}`, data);
    }

    putData(name:string, data:any, id:string){
        if(parseInt(id) === 0){
            return this.http.put(this.api + `/${name}`, data);
        }
        
        return this.http.put(this.api + `/${name}/${id}`, data);
    }

    changeState(name:string, id: string){
        return this.http.put(this.api + `/${name}/${id}/active`, '')
    }

    deleteData(name:string, id:string){
        return this.http.delete(this.api + `/${name}/${id}`);
    }


    /* Detalle */
    getDataPage(id: string, name: string){
        return this.http.get(this.api + `/module/${id}/${name}`);
    }

    getDataPageName(id: string){
        return this.http.get(this.api + `/module/${id}`);
    }

    /* Detalle */
    getTypeUser(type: string){
        return this.http.get(this.api + `/participant?fields=id,name&type=${type}`);
    }
}