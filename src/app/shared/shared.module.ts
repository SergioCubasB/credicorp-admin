import { ControlComponent } from './component/forms/control/control.component';
import { LabelComponent } from './component/forms/controls/label/label.component';
import { DropdownComponent } from './component/forms/controls/dropdown/dropdown.component';
import { CheckboxComponent } from './component/forms/controls/checkbox/checkbox.component';
import { TextInputComponent } from './component/forms/controls/text-input/text-input.component';
import { DynamicFormComponent } from './component/forms/dynamic-form/dynamic-form.component';
import { DoBootstrap, Inject, Injector, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "./material/material.module";
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { Ng7MatBreadcrumbModule } from "ng7-mat-breadcrumb";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { LoaderComponent } from './component/loader/loader.component';
import { HeaderComponent } from './component/header/header.component';
import { BannerComponent } from './component/banner/banner.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { DataTableComponent } from './component/data-table/data-table.component';
import { ViewComponent } from './component/modals/view/view.component';


import { CreateComponent } from "./component/forms/create/create.component";
import { DateInputComponent } from './component/forms/controls/date-input/date-input.component';
import { PasswordInputComponent } from './component/forms/controls/password-input/password-input.component';
import { DatetimeInputComponent } from './component/forms/controls/datetime-input/datetime-input.component';
import { ImageComponent } from './component/forms/controls/image/image.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { DropdownMultiplyComponent } from './component/forms/controls/dropdown-multiply/dropdown-multiply.component';
import { HtmlComponent } from './component/forms/controls/html/html.component';
import { FooterComponent } from './component/footer/footer.component';
import { DetailComponent } from './component/data-table/detail/detail.component';
import { FilesComponent } from './component/forms/controls/files/files.component';
import { BreadcrumbComponent } from './component/breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [ 
    LoaderComponent, 
    HeaderComponent, 
    BannerComponent, 
    SidebarComponent, 
    DataTableComponent, 
    ViewComponent,
    CreateComponent,

    /* Form */
    DynamicFormComponent,
    TextInputComponent,
    DateInputComponent,
    CheckboxComponent,
    DropdownComponent,
    LabelComponent,
    ControlComponent,
    PasswordInputComponent,
    DatetimeInputComponent,
    ImageComponent,
    DropdownMultiplyComponent,
    HtmlComponent,
    FooterComponent,
    DetailComponent,
    FilesComponent,
    BreadcrumbComponent
  ],
  imports: [
      MaterialModule,
      BrowserModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      CKEditorModule,
      Ng7MatBreadcrumbModule,
      
      CarouselModule,
      BrowserAnimationsModule,

      IonicModule.forRoot()
  ],
  exports: [
    LoaderComponent,
    HeaderComponent,
    BannerComponent,
    SidebarComponent,
    DataTableComponent,
    CreateComponent,
    
    /* Form */
    DynamicFormComponent,
    TextInputComponent,
    DateInputComponent,
    CheckboxComponent,
    DropdownComponent,
    LabelComponent,
    ControlComponent,
    HtmlComponent,
    FooterComponent,
    BreadcrumbComponent

  ]
})
export class SharedModule{}