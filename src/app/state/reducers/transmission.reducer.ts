import { addTransmission, loadedTransmissions } from './../actions/transmission.action';
import { TransmissionState } from './../../pages/dashboard/modules/transmission/interfaces/transmission.state';
import { createReducer, on } from "@ngrx/store";

export const initialState: TransmissionState = { loading: true, transmission: [] }

export const transmissionsReducer = createReducer(
    initialState,
    on( loadedTransmissions, (state) => {
        return { ...state, loading: false };
    }),
    on( loadedTransmissions, (state, { transmission }) => {
        return { ...state, loading: false, transmission };
    }),
    on( addTransmission, (state, { transmission } ) => {
        return { ...state, loading: false, transmission: [...state.transmission, transmission] };
    }),

)