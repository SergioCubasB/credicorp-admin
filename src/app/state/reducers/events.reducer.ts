import { loadEvents, loadedEvents, addEvent } from './../actions/events.actions';
import { createReducer, on } from "@ngrx/store";
import { EventState } from 'src/app/pages/dashboard/modules/events/interfaces/event.state';

export const initialState: EventState = { loading: true, event: [] }

export const eventsReducer = createReducer(
    initialState,
    on( loadEvents, (state) => {
        return { ...state, loading: false };
    }),
    on( loadedEvents, (state, { event }) => {
        return { ...state, loading: true, event };
    }),
    on( addEvent, (state, { event } ) => {
        return { ...state, loading: true, event: [...state.event, event] };
    }),

)