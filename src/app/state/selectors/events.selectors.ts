import { EventState } from './../../pages/dashboard/modules/events/interfaces/event.state';
import { state } from '@angular/animations';
import { createSelector } from '@ngrx/store';
import { AppState } from './../app.state';

export const selectorEvents = (state: AppState) => state.event;

export const selectIselectListEvents = createSelector(
    selectorEvents,
    (state: EventState) => state.event
);

export const selectLoading = createSelector(
    selectorEvents,
    (state: EventState) => state.loading
)