import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { TransmissionState } from 'src/app/pages/dashboard/modules/transmission/interfaces/transmission.state';

export const selectorTransmission = (state: AppState) => state.transmission;

export const selectIselectListTransmission = createSelector(
    selectorTransmission,
    (state: TransmissionState) => state.transmission
);

export const selectLoading = createSelector(
    selectorTransmission,
    (state: TransmissionState) => state.loading
)