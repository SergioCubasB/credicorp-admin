import { Transmission } from './../../pages/dashboard/modules/transmission/interfaces/transmission';
import { createAction, props } from "@ngrx/store";

const loadTransmission = createAction(
    '[Transmission list] Load transmissions'
)

export const loadedTransmissions = createAction(
    '[Transmission list] Loaded success',
    props<{ transmission: Transmission[] }>()
)

export const addTransmission = createAction(
    '[Transmission list] Transmission added success',
    props<{ transmission: Transmission }>()
)
