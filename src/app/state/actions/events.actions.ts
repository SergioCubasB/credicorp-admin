import { createAction, props } from "@ngrx/store";
import { Event } from "src/app/pages/dashboard/modules/events/interfaces/event";

export const loadEvents = createAction(
    '[Event list] Load events'
);

export const loadedEvents = createAction(
    '[Event list] Loaded success',
    props<{ event: Event[] }>()
)

export const addEvent = createAction(
    '[Event list] Event added success',
    props<{ event: Event }>()
)
