import { transmissionsReducer } from './reducers/transmission.reducer';
import { TransmissionState } from './../pages/dashboard/modules/transmission/interfaces/transmission.state';
import { eventsReducer } from './reducers/events.reducer';
import { ActionReducerMap } from "@ngrx/store";
import { EventState } from "../pages/dashboard/modules/events/interfaces/event.state";

export interface AppState{
    event: EventState;
    transmission: TransmissionState;
}

export const ROOT_REDUCERS: ActionReducerMap<AppState> = {
    event: eventsReducer,
    transmission: transmissionsReducer

}