import { SlidersComponent } from './dashboard/modules/sliders/sliders.component';
import { AuthInterceptorService } from './../auth/service/auth-interceptor.service';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../shared/material/material.module';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SearchFilterPipe } from "../pipes/search-filter.pipe";
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransmissionComponent } from './dashboard/modules/transmission/transmission.component';
import { EventsComponent } from './dashboard/modules/events/events.component';
import { ParticipantsComponent } from './dashboard/modules/participants/participants.component';
import { ReportsComponent } from './dashboard/modules/reports/reports.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ParametersComponent } from './dashboard/modules/parameters/parameters.component';
import { SocialComponent } from './dashboard/modules/social/social.component';
import { LinksComponent } from './dashboard/modules/links/links.component';
import { InternalPagesComponent } from './dashboard/modules/internal-pages/internal-pages.component';
import { UsersComponent } from './dashboard/modules/users/users.component';
import { LogoComponent } from './dashboard/modules/logo/logo.component';
import { EventDateComponent } from './dashboard/modules/event-date/event-date.component';

@NgModule({
  declarations: [
    PagesComponent,

    SearchFilterPipe,
    DashboardComponent,
    TransmissionComponent,
    EventsComponent,
    ParticipantsComponent,
    ReportsComponent,
    ParametersComponent,
    SocialComponent,
    SlidersComponent,
    LinksComponent,
    InternalPagesComponent,
    UsersComponent,
    LogoComponent,
    EventDateComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,

    AppRoutingModule,
    SharedModule,

    PagesRoutingModule,

    IonicModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, 
      useClass: AuthInterceptorService, 
      multi:true
    }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PagesModule { }
