import { EventDateComponent } from './dashboard/modules/event-date/event-date.component';
import { LogoComponent } from './dashboard/modules/logo/logo.component';
import { UsersComponent } from './dashboard/modules/users/users.component';
import { InternalPagesComponent } from './dashboard/modules/internal-pages/internal-pages.component';
import { SocialComponent } from './dashboard/modules/social/social.component';
import { LinksComponent } from './dashboard/modules/links/links.component';
import { ParametersComponent } from './dashboard/modules/parameters/parameters.component';
import { ReportsComponent } from './dashboard/modules/reports/reports.component';
import { ParticipantsComponent } from './dashboard/modules/participants/participants.component';
import { SlidersComponent } from './dashboard/modules/sliders/sliders.component';
import { AuthGuard } from './../auth/guard/auth.guard';
import { DetailComponent } from './../shared/component/data-table/detail/detail.component';
import { TransmissionComponent } from './dashboard/modules/transmission/transmission.component';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";


/* Componentes principales */
import { DashboardComponent } from "./dashboard/dashboard.component";
import { EventsComponent } from './dashboard/modules/events/events.component';

const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

            { path: 'eventos', component: EventsComponent, 
            data: {
                title: 'Eventos',
                breadcrumb: [ { label: 'Eventos', url: 'eventos' } ]
            }, canActivate: [AuthGuard] },

            { path: 'sliders', component: SlidersComponent, 
            data: {
                title: 'Sliders',
                breadcrumb: [ { label: 'Sliders', url: 'sliders' } ]
            }, canActivate: [AuthGuard] },

            { path: 'transmision', component: TransmissionComponent, 
            data: {
                title: 'Transmisiones',
                breadcrumb: [ { label: 'Transmisiones', url: 'transmision' } ]
            }, canActivate: [AuthGuard] },

            { path: 'participantes', component: ParticipantsComponent, 
            data: {
                title: 'Participantes',
                breadcrumb: [ { label: 'Participantes', url: 'participantes' } ]
            }, canActivate: [AuthGuard] },
            
            { path: 'reportes', component: ReportsComponent, 
            data: {
                title: 'Reportes',
                breadcrumb: [ { label: 'Reportes', url: 'reportes' } ]
            }, canActivate: [AuthGuard] },

            { path: 'password', component: TransmissionComponent, 
            data: {
                title: 'Password',
                breadcrumb: [ { label: 'Password', url: 'password' } ]
            }, canActivate: [AuthGuard] },

            { path: 'parameters', component: ParametersComponent, 
            data: {
                title: 'Parametros',
                breadcrumb: [ { label: 'Parametros', url: 'parameters' } ]
            }, canActivate: [AuthGuard] },

            { path: 'enlaces', component: LinksComponent, 
            data: {
                title: 'Enlaces',
                breadcrumb: [ { label: 'Enlaces', url: 'enlaces' } ]
            }, canActivate: [AuthGuard] },

            { path: 'redes-sociales', component: SocialComponent, 
            data: {
                title: 'Redes sociales',
                breadcrumb: [ { label: 'Redes sociales', url: 'redes-sociales' } ]
            }, canActivate: [AuthGuard] },

            { path: 'pagina', component: InternalPagesComponent, 
            data: {
                title: 'Paginas',
                breadcrumb: [ { label: 'Paginas', url: 'pagina' } ]
            }, canActivate: [AuthGuard] },

            { path: 'users', component: UsersComponent, 
            data: {
                title: 'Usuarios',
                breadcrumb: [ { label: 'Usuarios', url: 'users' } ]
            }, canActivate: [AuthGuard] },

            { path: 'logo', component: LogoComponent, 
            data: {
                title: 'Logos',
                breadcrumb: [ { label: 'Logos', url: 'logo' } ]
            }, canActivate: [AuthGuard] },

            { path: 'event-date', component: EventDateComponent, 
            data: {
                title: 'Días del evento',
                breadcrumb: [ { label: 'Fechas del evento', url: 'event-date' } ]
            }, canActivate: [AuthGuard] },


            { path: 'pagina/detalle/:id/:page', component: DetailComponent, 
            data: {
                title: 'Paginas',
                breadcrumb: [ 
                    {
                        label: 'Paginas',
                        url: './pagina'
                    },
                    {
                    label: '{{detail}} / detalle',
                    url: ''
                    }
                ]
            }, canActivate: [AuthGuard] },

            { path: 'pagina/atributo/:id/:page', component: DetailComponent, 
            data: {
                title: 'Paginas',
                breadcrumb: [ 
                    {
                        label: 'Paginas',
                        url: './pagina'
                    },
                    {
                    label: '{{detail}} / atributos',
                    url: ''
                    }
                ]
            }, canActivate: [AuthGuard] },

            { path: 'pagina/atributo-externo/:id/:page', component: DetailComponent, 
            data: {
                title: 'Paginas',
                breadcrumb: [ 
                    {
                        label: 'Paginas',
                        url: './pagina'
                    },
                    {
                    label: '{{detail}} / atributos externos',
                    url: ''
                    }
                ]
            }, canActivate: [AuthGuard] },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule {}