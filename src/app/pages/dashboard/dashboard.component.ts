import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ width: 0, opacity: 0 }),
            animate('.3s ease-out', 
                    style({ width: 100, opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ width: 100, opacity: 1 }),
            animate('.3s ease-in', 
                    style({ width: 0, opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class DashboardComponent implements OnInit {
  isLoading: boolean = true;
  isShowSideBar: boolean = true;
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.isLoading = false;
    }, 2000);
    
  }

  showSideBar(state: boolean){
    this.isShowSideBar = state;
    console.log("hola",state);
  }

}
