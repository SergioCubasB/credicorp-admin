import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { selectIselectListTransmission } from './../../../../state/selectors/transmission.selectors';
import { loadedTransmissions } from './../../../../state/actions/transmission.action';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { ParametersService } from './../parameters/services/parameters.service';
import { TransmissionService } from './services/transmission.service';
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-transmission',
  templateUrl: './transmission.component.html',
  styleUrls: ['./transmission.component.scss']
})
export class TransmissionComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];

  types: any[] = [];
  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private transmissionService: TransmissionService,
    private parametersService: ParametersService

  ) { }

  ngOnInit(): void {
    this.store.dispatch(loadEvents());
    
    this.getTransmission();
    this.myColumns = this.getColumns();
    this.getMeta();

  }

  getTransmission(){
    this.transmissionService.getTransmissions()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
      }
    )
  }


  getColumns():  any[] {
    return [
      { caption: 'Url español', field: 'url_principal_es' },
      { caption: 'Url ingles', field: 'url_principal_en' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '600px',
      data: {
        title: "Crear transmisión",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'transmission'
      }
    });
    
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Url:",
            "controlType": "textinput",
            "key": "url_principal_es",
            "required": true,
            "order": 2
          },
          {
            "label": "Url alternativo:",
            "controlType": "textinput",
            "key": "url_alternative_es",
            "required": true,
            "order": 2
          },
          {
            "label": "Tipo:",
            "key": "tipo",
            "options": 'TYPE_MEET',
            "order": 8,
            "controlType": "dropdown"
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Url:",
            "controlType": "textinput",
            "key": "url_principal_en",
            "required": true,
            "order": 2
          },
          {
            "label": "Url alternativo:",
            "controlType": "textinput",
            "key": "url_alternative_en",
            "required": true,
            "order": 2
          }
        ]
      }             
    ];
  }

}
