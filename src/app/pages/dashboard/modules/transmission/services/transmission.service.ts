import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TransmissionService {
    private env = environment;
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getTransmissions(){
        return this.http.get(this.api + '/transmission?expand=EventsbySpeaker,EventsbyModerator');
    }

    getTransmission(id: string){
        return this.http.get(this.api + `/transmission/${id}`);
    }

    deleteTransmission(id:string){
        return this.http.delete(this.api + `/transmission/${id}`);
    }

}