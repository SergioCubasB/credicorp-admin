export interface Transmission{
    id: number,
    url_principal_es: string,
    url_principal_en: string,
    url_alternative_es: string,
    url_alternative_en: string,
    tipo: number,
    status: number
}