import { Transmission } from "./transmission";

export interface TransmissionState {
    loading: boolean,
    transmission: ReadonlyArray<Transmission>;
}