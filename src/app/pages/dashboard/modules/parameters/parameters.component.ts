import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { ParametersService } from './services/parameters.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss']
})
export class ParametersComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];
  estadoData: boolean = false;

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,
    
    private parametersService: ParametersService
  ) { }

  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getParameters();
    this.myColumns = this.getColumns();
    this.getMeta();
  }

  getParameters(){
    this.parametersService.getParameters()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
      }
    )
  }

  getColumns():  any[] {
    return [
      { caption: 'Grupo', field: 'group' },
      { caption: 'Nombre', field: 'name' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '400px',
      data: {
        title: "Crear parametro",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'parameter'
      }
    });
    
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Grupo:",
            "controlType": "textinput",
            "key": "group",
            "required": true,
            "order": 1
          },
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

}
