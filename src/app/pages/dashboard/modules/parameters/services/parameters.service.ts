import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ParametersService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getParameters(){
        return this.http.get(this.api + '/parameter');
    }

    getGroup(name: string){
        return this.http.get(this.api + `/parameter?group=${name}`);
    }

    getParameter(id: string){
        return this.http.get(this.api + `/parameter/${id}`);
    }

    deleteParameter(id:string){
        return this.http.delete(this.api + `/parameter/${id}`);
    }

}