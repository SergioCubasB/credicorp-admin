import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LinkService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getLinks(){
        return this.http.get(this.api + '/homefooter');
    }

    getLink(id: string){
        return this.http.get(this.api + `/homefooter/${id}`);
    }

    deleteLink(id:string){
        return this.http.delete(this.api + `/homefooter/${id}`);
    }

}