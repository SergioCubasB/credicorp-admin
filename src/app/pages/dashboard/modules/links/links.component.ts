import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LinkService } from './services/link.service';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
  actions: any =  { "editar": true, "eliminar": false }

  myData: any
  myColumns: any;
  meta: any[] = [];

  type_social: any[] = [];

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private linkService: LinkService

  ) { }
  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getData();

    this.myColumns = this.getColumns();

    this.getMeta();    
  }
  
  getData(){
    
    this.linkService.getLinks()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
      }
    )
  }

  createElement(){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '400px',
      data: {
        title: "Crear link",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'homefooter'
      }
    });
  }

  getColumns():  any[] {
    return [
      { caption: 'Nombre', field: 'name' },
      { caption: 'URL', field: 'url' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 1
          },
          {
            "label": "URL:",
            "controlType": "textinput",
            "key": "url",
            "required": true,
            "order": 2
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name_en",
            "required": true,
            "order": 1
          }
        ]
      }
    ];
  }


}
