import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SocialService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getSocials(){
        return this.http.get(this.api + '/social-network');
    }

    getSocial(id: string){
        return this.http.get(this.api + `/social-network/${id}`);
    }

    deleteSocial(id:string){
        return this.http.delete(this.api + `/social-network/${id}`);
    }

}