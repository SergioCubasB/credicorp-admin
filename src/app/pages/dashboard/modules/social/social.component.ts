import { loadedEvents, loadEvents } from './../../../../state/actions/events.actions';
import { selectLoading } from './../../../../state/selectors/events.selectors';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { SocialService } from './services/social.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];

  type_social: any[] = [];

  loading$: Observable<boolean> = new Observable(); 
  constructor(
    public dialog: MatDialog,
    private store: Store<AppState>,

    private socialService: SocialService
  ) { }

  ngOnInit(): void {
    this.loading$ = this.store.select(selectLoading);

    this.store.dispatch(loadEvents());

    this.getData();

    this.myColumns = this.getColumns();

    this.getMeta();    
  }
  
  getData(){
    
    this.socialService.getSocials()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
      }
    )
  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '350px',
      data: {
        title: "Crear red social",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'social-network'
      }
    });
  }

  getColumns():  any[] {
    return [
      { caption: 'Nombre', field: 'name' },
      { caption: 'URL', field: 'url' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 1
          },
          {
            "label": "URL:",
            "controlType": "textinput",
            "key": "url",
            "required": true,
            "order": 2
          },
          {
            "label": "Tipo:",
            "key": "type_social_network_id",
            "options": 'TYPE_SOCIAL_NETWORKS',
            "order": 2,
            "controlType": "dropdown"
          },
        ]
      }
    ];
  }

}
