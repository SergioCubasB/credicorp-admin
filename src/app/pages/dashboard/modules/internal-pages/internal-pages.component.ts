import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { PagesService } from './services/pages.service';
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-internal-pages',
  templateUrl: './internal-pages.component.html',
  styleUrls: ['./internal-pages.component.scss']
})
export class InternalPagesComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];
  estadoData: boolean = false;
  actions: any =  { "editar": true, "eliminar": false }
  

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private pagesService: PagesService
  ) { }

  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getPages();

    this.myColumns = this.getColumns();
    this.getMeta();
  }

  getPages(){
    this.pagesService.getPages()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
        
        this.estadoData = true;
      }
    )
  }


  getColumns():  any[] {
    return [
      { caption: 'Nombre', field: 'name' },
      { caption: 'Acciones', field: 'acciones' },
      { caption: 'Opciones', field: 'interno' },
    ];
  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '500px',
      data: {
        title: "Crear pagina",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'module'
      }
    });
    
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 2
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name_en",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

}
