import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class PagesService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getPages(){
        return this.http.get(this.api + '/module');
    }

    getPage(id: string){
        return this.http.get(this.api + `/module/${id}`);
    }

    deletePage(id:string){
        return this.http.delete(this.api + `/module/${id}`);
    }

}