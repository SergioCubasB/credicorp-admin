import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    private env = environment;
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getUsers(){
        return this.http.get(this.api + '/user-admin?expand=file&?fields=id,name,last_name,email');
    }

    getUser(id: string){
        return this.http.get(this.api + `/user-admin/${id}?expand=file`);
    }

    deleteUser(id:string){
        return this.http.delete(this.api + `/user-admin/${id}`);
    }

}