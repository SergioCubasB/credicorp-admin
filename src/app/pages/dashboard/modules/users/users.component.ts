import { UsersService } from './services/users.service';
import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { CreateComponent } from 'src/app/shared/component/forms/create/create.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  myData: any
  myColumns: any;

  types: any[] = [];
  nationality: any[] = [];

  meta: any[] = [];

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private usersService: UsersService,
  ){}


  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getEvents();
    this.myColumns = this.getColumns();
    this.getMeta();    
  }

  getEvents(){
    this.usersService.getUsers()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
      }
    )
  }

  getColumns():  any[] {
    return [
      { caption: 'Nombre', field: 'name' },
      { caption: 'Apellidos', field: 'last_name' },
      { caption: 'Documento', field: 'number_document' },
      { caption: 'Email', field: 'user' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '400px',
      panelClass: 'h-80',
      data: {
        title: "Crear usuario",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'user-admin'
      }
    });
    
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 1
          },
          {
            "label": "Apellido:",
            "controlType": "textinput",
            "key": "last_name",
            "required": true,
            "order": 2
          },
          {
            "label": "Documento:",
            "controlType": "textinput",
            "key": "number_document",
            "required": true,
            "order": 3
          },
          {
            "label": "Email:",
            "controlType": "textinput",
            "key": "user",
            "required": true,
            "order": 4
          },
          {
            "label": "Password:",
            "controlType": "passwordInput",
            "key": "password",
            "required": true,
            "order": 5
          },
          {
            "label": "Foto:",
            "controlType": "imageInput",
            "key": "file",
            "required": true,
            "order": 6
          }
        ]
      }         
    ];
    
  }

}
