import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { Store } from '@ngrx/store';
import { AppState } from './../../../../state/app.state';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

/* Componentes internos*/
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';

/* Servicios */
import { SlidersService } from './services/sliders.service';

@Component({
  selector: 'app-sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.scss']
})
export class SlidersComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private slidersService: SlidersService
  ) { }

  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getSliders();

    this.myColumns = this.getColumns();

    this.getMeta();    
  }

  getSliders(){
    this.slidersService.getSliders()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.myData = data;
        this.store.dispatch(loadedEvents( { event: data }));

      }
    )
  }

  createElement(){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '600px',
      data: {
        title: "Crear Slider",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'homeheader'
      }
    });
  }

  getColumns():  any[] {
    return [
      //{ caption: 'Nombre', field: 'name' },
      { caption: 'Imagen', field: 'file' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }


  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          /*{
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 2
          },*/
          {
            "label": "Imagen:",
            "controlType": "imageInput",
            "key": "file",
            "required": true,
            "order": 2
          },
          {
            "label": "Tipo:",
            "key": "type_head_id",
            "options": 'TYPE_HEADER',
            "order": 3,
            "controlType": "dropdown"
          },
          {
            "label": "Url:",
            "controlType": "textinput",
            "key": "url",
            "required": true,
            "order": 4
          }
        ]
      },
      {
        language: "EN",
        data: [
          /*{
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name_en",
            "required": true,
            "order": 1
          },*/
          {
            "label": "Imagen:",
            "controlType": "imageInput",
            "key": "file_en",
            "required": true,
            "order": 2
          }
        ]
      } 
    ];
  }
}
