import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SlidersService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getSliders(){
        return this.http.get(this.api + '/homeheader');
    }

    getSlider(id: string){
        return this.http.get(this.api + `/homeheader/${id}`);
    }

    deleteSlider(id:string){
        return this.http.delete(this.api + `/homeheader/${id}`);
    }

}