import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { loadedEvents, loadEvents } from './../../../../state/actions/events.actions';
import { LogoService } from './services/logo.service';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {
  actions: any =  { "editar": true, "eliminar": false }

  myData: any
  myColumns: any;
  meta: any[] = [];

  type_social: any[] = [];

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private logoService: LogoService

  ) { }
  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getData();

    this.myColumns = this.getColumns();

    this.getMeta();    
  }
  
  getData(){
    
    this.logoService.getLogos()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));
        this.myData = data;
      }
    )
  }

  createElement(){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '400px',
      data: {
        title: "Crear logo",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'logo'
      }
    });
  }

  getColumns():  any[] {
    return [
      { caption: 'Imagen', field: 'file' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  
  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Imagen:",
            "controlType": "imageInput",
            "key": "file",
            "required": true,
            "order": 1
          },
          {
            "label": "Imagen_en:",
            "controlType": "imageInput",
            "key": "file_en",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

}
