import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LogoService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getLogos(){
        return this.http.get(this.api + '/logo');
    }

    getLogo(id: string){
        return this.http.get(this.api + `/logo/${id}`);
    }

    deleteLogo(id:string){
        return this.http.delete(this.api + `/logo/${id}`);
    }

}