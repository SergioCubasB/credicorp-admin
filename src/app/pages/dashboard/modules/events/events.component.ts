import { selectIselectListEvents } from './../../../../state/selectors/events.selectors';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { EventsService } from './services/events.service';
import { Component, OnInit } from '@angular/core';
import { CreateComponent } from 'src/app/shared/component/forms/create/create.component';
import { loadedEvents, loadEvents } from 'src/app/state/actions/events.actions';
import { Observable } from 'rxjs';
import { selectLoading } from 'src/app/state/selectors/events.selectors';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  myData: any
  myColumns: any;
  meta: any[] = [];

  types: any[] = [];
  speakers: any[] = [];
  moderators: any[] = [];
  nationality: any[] = [];

  loading$: Observable<boolean> = new Observable(); 

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private eventsService: EventsService
  ){}

  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getEvents();
    this.myColumns = this.getColumns();
  }

  getEvents(){
    this.loading$ = this.store.select(selectLoading);

    this.eventsService.getEvents()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        
        this.store.dispatch(loadedEvents( { event: data }));

        this.store.select(selectIselectListEvents)
        .subscribe(
          (events) => {
            this.myData = events;
          }
        )

        this.getMeta();
      }
    )
  }


  createElement(){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '700px',
      panelClass: 'h-80',
      data: {
        title: "Crear evento",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'event'
      }
    });
    
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Titulo:",
            "controlType": "textinput",
            "key": "title",
            "required": true,
            "order": 1
          },
          {
            "label": "Descripción:",
            "controlType": "textinput",
            "key": "description",
            "required": true,
            "order": 2
          },
          /*{
            "label": "Fecha:",
            "controlType": "dateinput",
            "key": "date",
            "required": true,
            "order": 3
          },*/
          {
            "label": "Fecha:",
            "controlType": "datetimeinput",
            "key": "date",
            "required": true,
            "order": 3
          },
          {
            "label": "Ciudad:",
            "controlType": "textinput",
            "key": "city",
            "required": true,
            "order": 4,
          },
          {
            "label": "Tipo:",
            "key": "type_id",
            "options": 'TYPE_MEET',
            "order": 5,
            "controlType": "dropdown"
          },
          {
            "label": "Oradoras:",
            "key": "speakers",
            "options": 'ORADORES',
            "order": 6,
            "controlType": "dropdown-multiply",
            "required": true,
          },
          {
            "label": "Moderadoras:",
            "key": "moderators",
            "options": 'MODERATORS',
            "order": 7,
            "controlType": "dropdown-multiply",
            "required": true,
          },
          {
            "label": "Documentos:",
            "key": "documents",
            "options": 'FILE',
            "order": 8,
            "controlType": "files",
            "required": true,
          },
          {
            "label": "Url Presentación:",
            "controlType": "textinput",
            "key": "presentations[0][url]",
            "required": true,
            "order": 9,
          },
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Titulo:",
            "controlType": "textinput",
            "key": "title_en",
            "required": true,
            "order": 1
          },
          {
            "label": "Url Presentación:",
            "controlType": "textinput",
            "key": "presentations[0][url_en]",
            "required": true,
            "order": 2,
          },
        ]
      }             
    ];

  }

  getColumns():  any[] {
    return [
      { caption: 'Titulo', field: 'title' },
      { caption: 'Descripcion', field: 'description' },
      { caption: 'Fecha', field: 'date' },
      { caption: 'Acciones', field: 'acciones' },
      { caption: 'Estado', field: 'active' }
    ];
  }
  
}
