import { Event } from "./event";

export interface EventState {
    loading: boolean,
    event: ReadonlyArray<Event>;
}