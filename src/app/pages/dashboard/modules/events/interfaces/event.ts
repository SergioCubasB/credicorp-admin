export interface Event{
    id: number,
    title: string,
    title_en: string,
    description: string,
    date: string,
    city: string,
    type_id: number,
    condition: number,
    date_string: string,
    date_string_en: string,
    date_string_large: string,
    date_string_large_en: string,
    active: number
}