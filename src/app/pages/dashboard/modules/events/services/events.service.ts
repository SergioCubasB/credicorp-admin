import { environment } from './../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class EventsService {
    private env = environment;
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getEvents(){
        return this.http.get(this.api + '/event?fields=id,title,description,date,date_string');
    }

    getUsers(type: string){
        return this.http.get(this.api + `/participant?type=${type}`);
    }

    getEvent(id: string){
        return this.http.get(this.api + `/event/${id}`);
    }

    deleteEvent(id:string){
        return this.http.delete(this.api + `/event/${id}`);
    }

}