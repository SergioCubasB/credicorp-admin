import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class EventDateService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getEventDates(){
        return this.http.get(this.api + '/event-date');
    }

    getEventDate(id: string){
        return this.http.get(this.api + `/event-date/${id}`);
    }

    deleteEventDate(id:string){
        return this.http.delete(this.api + `/event-date/${id}`);
    }

}