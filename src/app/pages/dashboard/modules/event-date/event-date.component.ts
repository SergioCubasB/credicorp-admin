import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { EventDateService } from './services/event-date.service';
import { loadedEvents, loadEvents } from './../../../../state/actions/events.actions';
import { CreateComponent } from './../../../../shared/component/forms/create/create.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-event-date',
  templateUrl: './event-date.component.html',
  styleUrls: ['./event-date.component.scss']
})
export class EventDateComponent implements OnInit {
  actions: any =  { "editar": true, "eliminar": false }

  myData: any
  myColumns: any;
  meta: any[] = [];

  type_social: any[] = [];

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private eventDateService: EventDateService

  ) { }
  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getData();

    this.myColumns = this.getColumns();

    this.getMeta();    
  }
  
  getData(){
    
    this.eventDateService.getEventDates()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));
        console.log("DAAAA:",data);
        
        this.myData = data;
      }
    )
  }

  createElement(){
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '400px',
      data: {
        title: "Crear",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'event-date'
      }
    });
  }

  getColumns():  any[] {
    return [
      { caption: 'Fecha inicio', field: 'start_date' },
      { caption: 'Fecha fin', field: 'end_date' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  
  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Fecha inicio:",
            "controlType": "dateinput",
            "key": "start_date",
            "required": false,
            "order": 1
          },
          {
            "label": "Fecha fin:",
            "controlType": "dateinput",
            "key": "end_date",
            "required": true,
            "order": 2
          }
        ]
      }
    ];
  }

}
