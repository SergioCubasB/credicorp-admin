import { environment } from '../../../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ParticipantsService {
    private env = environment;
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}/v1`;
    }

    getParticipants(){
        return this.http.get(this.api + '/participant?expand=file&?fields=id,name,last_name,email');
    }

    getParticipant(id: string){
        return this.http.get(this.api + `/participant/${id}?expand=file`);
    }

    deleteParticipant(id:string){
        return this.http.delete(this.api + `/participant/${id}`);
    }

}