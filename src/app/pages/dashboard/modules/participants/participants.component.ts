import { loadEvents, loadedEvents } from './../../../../state/actions/events.actions';
import { AppState } from './../../../../state/app.state';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { CreateComponent } from 'src/app/shared/component/forms/create/create.component';
import { ParticipantsService } from './services/participants.service';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss']
})
export class ParticipantsComponent implements OnInit {
  myData: any
  myColumns: any;

  types: any[] = [];
  nationality: any[] = [];

  meta: any[] = [];

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog,

    private participantsService: ParticipantsService,
  ){}


  ngOnInit(): void {
    this.store.dispatch(loadEvents());

    this.getEvents();
    this.myColumns = this.getColumns();
    this.getMeta();    
  }

  getEvents(){
    this.participantsService.getParticipants()
    .subscribe(
      (response:any) => {
        const { data } = response.data;
        this.store.dispatch(loadedEvents( { event: data }));

        this.myData = data;
      }
    )
  }

  getColumns():  any[] {
    return [
      { caption: 'Imagen', field: 'file' },
      { caption: 'Nombre', field: 'name' },
      { caption: 'Apellidos', field: 'last_name' },
      { caption: 'Email', field: 'email' },
      { caption: 'Acciones', field: 'acciones' }
    ];
  }

  createElement(){
    
    const dialogRef = this.dialog.open(CreateComponent, {
      width: '600px',
      panelClass: 'h-80',
      data: {
        title: "Crear participante",
        meta: this.meta,
        buttonText: "Crear",
        type: 'POST',
        service: 'participant'
      }
    });
    
  }

  getMeta(){
    this.meta = [
      {
        language: "ES",
        data: [
          {
            "label": "Nombre:",
            "controlType": "textinput",
            "key": "name",
            "required": true,
            "order": 1
          },
          {
            "label": "Apellido:",
            "controlType": "textinput",
            "key": "last_name",
            "required": true,
            "order": 2
          },
          {
            "label": "Email:",
            "controlType": "textinput",
            "key": "email",
            "required": true,
            "order": 3
          },
          {
            "label": "Nacionalidad:",
            "key": "nationality_id",
            "options": 'NATIONALITY',
            "order": 4,
            "controlType": "dropdown"
          },
          {
            "label": "Compania:",
            "controlType": "textinput",
            "key": "company",
            "required": true,
            "order": 5
          },
          {
            "label": "Cargo:",
            "controlType": "textinput",
            "key": "position",
            "required": true,
            "order": 6
          },
          {
            "label": "Descripción:",
            "controlType": "textinput",
            "key": "description",
            "required": true,
            "order": 7
          },
          {
            "label": "Tipo:",
            "key": "type_id",
            "options": 'TYPE_PARTICIPANT',
            "order": 8,
            "controlType": "dropdown",
            "required": false,
          },
          {
            "label": "Imagen:",
            "controlType": "imageInput",
            "key": "file",
            "required": true,
            "order": 9
          }
        ]
      },
      {
        language: "EN",
        data: [
          {
            "label": "Cargo:",
            "controlType": "textinput",
            "key": "position_en",
            "required": true,
            "order": 2
          },
          {
            "label": "Descripción:",
            "controlType": "textinput",
            "key": "description_en",
            "required": true,
            "order": 2
          }
        ]
      }             
    ];
    
  }

}
